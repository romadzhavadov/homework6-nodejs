import { BaseController } from './base.controller';

export class NewsController extends BaseController {
  constructor() {
    super('newsposts');
  }
  
}

export default NewsController;

