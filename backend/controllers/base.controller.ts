import { Request, Response } from 'express';
import { DatabaseService } from '../services';

// Клас BaseController є базовим класом для всіх контролерів.
export class BaseController {
    private table: string;

    // Конструктор класу BaseController, приймає назву таблиці як аргумент та зберігає її в приватній властивості table.
    constructor(table: string) {
        this.table = table;

        // Прив'язка контексту для кожного методу класу, щоб забезпечити правильний контекст `this` при виклику цих методів.
        // Це необхідно, оскільки методи будуть передаватися як обробники подій у маршрутах Express.
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
    }

    // Метод для створення нового запису у базі даних для вказаної таблиці.
    async getItems (req: Request, res: Response) {
        try {

            // const limit = Number(req.query.limit) || 4
            // const skip = Number(req.query.skip) || 0

            const data = await DatabaseService.readAll(this.table, 
            //     {
            //     limit,
            //     skip,
            // }
            );

            res.status(200).json({ data });

            if (!data) {
                res.status(200).json([]); 
            }
        } catch (error) {
            console.error("Error find news:", error);
            res.status(500).json({ error: "Error find news" }); 
        }
    }
    
    async getOneItems (req: Request, res: Response) {
        try {
            const data = await DatabaseService.read(this.table, Number(req.params.id));

            if (data) {
                res.status(200).json({ data, message: `${this.table} read` });
            } else {
                res.status(404).json({ errorMessage: "News not found" });
            }
        } catch (error) {
            console.error("Error find news:", error);
            res.status(500).json({ error: "Error find news" });
        }
    }
    
    async addNews (req: Request, res: Response) {
        try {
            const data = await DatabaseService.create(this.table, req.body);

            res.status(201).json({ message: `${this.table} created`, data });
        } catch (error) {
            console.error("Error adding news:", error);
            res.status(500).json({ error: "Error adding news" }); 
        }
    }
    
    async updateNews (req: Request, res: Response) {
        try {
            const data = await DatabaseService.read(this.table, Number(req.params.id));
            if (!data) {
                res.status(404).json({ errorMessage: "News not found" });
            }
            const updateData = await DatabaseService.update(this.table, Number(req.params.id), req.body);

            res.status(200).json({ updateData, message: `${this.table} updated` });

        } catch (error) {
            console.error("Error updating news:", error);
            res.status(500).json({ error: "Error updating news" });
        }
        }
    
    async deleteNews (req: Request, res: Response) {
        try {
            const data = await DatabaseService.read(this.table, Number(req.params.id));
            if (!data) {
                res.status(404).json({ errorMessage: "News not found" });
            }
            await DatabaseService.delete(this.table, Number(req.params.id));
            res.status(200).json({ message: `${this.table} was  removed` });
        } catch (error) {
          console.error("Error deleting news:", error);
          res.status(500).json({ error: "Error deleting news" });
        }
      }
}