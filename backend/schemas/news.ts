export default {
    name: 'newsposts',
    fields: [
        {
            name: 'id',
            type: 'increments'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'description',
            type: 'string'
        }
    ]
}