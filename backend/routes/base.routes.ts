import { Router } from 'express';
import { BaseController } from '../controllers/base.controller';


class BaseRouter {
  public router: Router;
  private controller: BaseController;

  constructor(controller: BaseController) {
    this.router = Router();
    this.controller = controller;

    this.routes()
  }

  routes() {
    this.router.route('/')
      .post(this.controller.addNews)
      .get(this.controller.getItems);

    this.router.route('/:id')
      .get(this.controller.getOneItems)
      .put(this.controller.updateNews)
      .delete(this.controller.deleteNews);
    }
}


export default BaseRouter;