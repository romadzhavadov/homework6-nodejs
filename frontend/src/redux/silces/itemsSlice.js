import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: [],
  loading: false,
  error: null,
}

const itemsSlice = createSlice({
  name: "items",
  initialState,

  reducers: {
    setItems: (state, action) => {
      state.items = action.payload;
    },

    setLoading(state, action) {
      state.loading = action.payload;
    },

    setError(state, action) {
      state.error = action.payload;
    }
  }
});

export const { setItems, setLoading, setError} = itemsSlice.actions;

export const fetchItems = () => async (dispatch) => {
  dispatch(setLoading(true));

  try {
    const res = await fetch(`http://localhost:8000/api/newsposts`);

    if (!res.ok) {
      throw new Error('Failed to fetch items');
    }
    const { data } = await res.json();

    dispatch(setItems(data.items));
  } catch (error) {
    console.error('Error fetching items:', error);
    dispatch(setError(error.message));
  } finally {
    dispatch(setLoading(false));
  }
}

export default itemsSlice.reducer;

