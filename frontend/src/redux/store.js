import { configureStore } from "@reduxjs/toolkit";
import itemsReducer from "./silces/itemsSlice";
import newsReducer from "./silces/newsSlice"

const store = configureStore({
  reducer: {
    items: itemsReducer,
    news: newsReducer
  },
})

export default store;