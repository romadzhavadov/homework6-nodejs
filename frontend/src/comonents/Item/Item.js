import PropTypes from "prop-types";
import styles from './Item.module.scss';
import { Link } from 'react-router-dom';

const Item = ({ id, name, description }) => {

  return (
    <Link to={`/${id}`}>
      <div className={styles.root}>
        <div className={styles.contentWrapper}>
          <div className={styles.info}>
            <div className={styles.name}>{name}</div>
            <div className={styles.description}>{description}</div>
          </div>
        </div>
      </div>
    </Link>
  )
}

Item.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  name: PropTypes.string,
  description: PropTypes.string,
};

Item.defaultProps = {
  id: '',
  name: '',
  description: '',
}

export default Item;