import styles from './ItemsContainer.module.scss';
import Item from '../Item/Item';
import {shallowEqual, useSelector } from "react-redux";


const ItemsContainer = () => {

  const items = useSelector(state => state.items.items, shallowEqual);

  return (

    <div className={styles.root}>
      {items 
        ? 
        items?.map(item => <Item key={item.id} {...item} />) 
        : 
        (<p style={{textAlign: 'center'}}>Завантаження...</p>)
      }

    </div>
  )
}

export default ItemsContainer;