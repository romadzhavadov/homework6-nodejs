import './App.scss';
import Header from "./comonents/Header";
import AppRoutes from "./AppRoutes";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { fetchItems } from "./redux/silces/itemsSlice";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchItems())
  }, []);
  
  return (
    <div className="App">
        <Header />
        <AppRoutes />
    </div>
  );
}

export default App;
